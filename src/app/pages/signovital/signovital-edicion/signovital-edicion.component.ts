import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map,startWith} from 'rxjs/operators';
import { Paciente } from 'src/app/_model/Paciente';
import { SignoVital } from 'src/app/_model/SignoVital';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignovitalService } from 'src/app/_service/signovital.service';
import { PacienteEdicionComponent } from '../../paciente/paciente-edicion/paciente-edicion.component';

@Component({
  selector: 'app-signovital-edicion',
  templateUrl: './signovital-edicion.component.html',
  styleUrls: ['./signovital-edicion.component.css']
})
export class SignovitalEdicionComponent implements OnInit {

  id:number;
  form:FormGroup;
  edicion:boolean = false;
  objSignoVital:SignoVital;
  //fecha
  maxFecha: Date = new Date();

  //AutoComplete
  filteredOptions: Observable<any[]>;

  //pacientes
  pacienteSeleccionado: Paciente;
  listaPacientes:Paciente[] = null;

  myControlPaciente: FormControl = new FormControl();
  
  constructor(private builder: FormBuilder,
              private pacienteService:PacienteService,
              private signovService:SignovitalService,
              private route:ActivatedRoute,
              private router:Router,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.form = this.builder.group({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
    });
    this.listarPacientes();
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));

    this.objSignoVital = new SignoVital();

    //revisar video
    this.route.params.subscribe((params:Params) =>{
        this.id = params['id'];
        this.edicion = this.id != null;
        this.initForm();
    })
    console.log( this.edicion);

    this.pacienteService.pacienteCambio.subscribe(data =>{
        this.listaPacientes = data;
    })

  }

  listarPacientes(){
    this.pacienteService.listar().subscribe(data =>{
          this.listaPacientes = data;
    });
  }

  seleccionarPaciente(e: any){
    //console.log(e);
    this.pacienteSeleccionado = e.option.value;
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.listaPacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.listaPacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  
  initForm(){
    if(this.edicion){
      
        this.signovService.listarSignoVitalPorId(this.id).subscribe(data =>{
          this.form = this.builder.group({
            'id': new FormControl(data.idSigno),
            'paciente': new FormControl(data.paciente),
            'fecha': new FormControl(data.fecha),
            'temperatura': new FormControl(data.temperatura),
            'pulso': new FormControl(data.pulso),
            'ritmo': new FormControl(data.ritmo)
          });
        });
    }
  }

  Operar(){
    this.objSignoVital.idSigno = this.form.value['id'];
    this.objSignoVital.paciente = this.form.value['paciente'];
    //fecha
    var tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString()
    this.objSignoVital.fecha = localISOTime;

    this.objSignoVital.temperatura = this.form.value['temperatura'];
    this.objSignoVital.pulso = this.form.value['pulso'];
    this.objSignoVital.ritmo = this.form.value['ritmo'];

    if (this.edicion){
      //actualiza
      this.signovService.actualizar(this.objSignoVital).subscribe(actSignoV =>{
          this.signovService.listar().subscribe(listSignoV =>{
              this.signovService.signovitalCambio.next(listSignoV);
              this.signovService.mensajeCambio.next('Se Actualizo');
          });
      });
    } else {
      //registro
       this.signovService.registrar(this.objSignoVital).subscribe(regSignoV =>{
          this.signovService.listar().subscribe(listSignoV =>{
              this.signovService.signovitalCambio.next(listSignoV);
              this.signovService.mensajeCambio.next('Se registro');   
          });
       });
    }
    this.limpiarControles();
    this.router.navigate(['signovital']);
  }

  limpiarControles() {
    this.pacienteSeleccionado = null;
    this.listaPacientes = [];
  }

  openDialog(){
    let pac =  new Paciente();
    this.dialog.open(PacienteEdicionComponent,{
      width: '500px',
      disableClose:false,
      data : pac
    });
  }
}
