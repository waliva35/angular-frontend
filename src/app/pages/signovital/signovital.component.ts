import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SignoVital } from 'src/app/_model/SignoVital';
import { SignovitalService } from 'src/app/_service/signovital.service';

@Component({
  selector: 'app-signovital',
  templateUrl: './signovital.component.html',
  styleUrls: ['./signovital.component.css']
})
export class SignovitalComponent implements OnInit {

  dataSource:MatTableDataSource<SignoVital>;
  displayedColumns = ['idSigno','paciente','fecha','temperatura','pulso','ritmo','acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private signovService:SignovitalService,
              private dialog:MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.signovService.listar().subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.signovService.signovitalCambio.subscribe(listSignoV =>{
        this.dataSource = new MatTableDataSource(listSignoV);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    })

    this.signovService.mensajeCambio.subscribe(mensaje =>{
        this.snackBar.open(mensaje,'AVISO',{duration:2000});
    })
  }

  eliminar(idSigno:number){
    this.signovService.eliminar(idSigno).subscribe(elimSignoV =>{
        this.signovService.listar().subscribe(listSignov =>{
            this.signovService.signovitalCambio.next(listSignov);
            this.signovService.mensajeCambio.next('Se elimino');
        })
    })
  }


}
