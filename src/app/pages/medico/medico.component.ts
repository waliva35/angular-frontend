import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Medico } from 'src/app/_model/Medico';
import { MedicoService } from 'src/app/_service/medico.service';
import { DialogoComponent } from './dialogo/dialogo.component';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  dataSource:MatTableDataSource<Medico>;
  displayedColumns = ['idMedico','nombres','apellidos','acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private medicoService:MedicoService,
              private dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {

    this.medicoService.medicoCambio.subscribe(data =>{
      this.dataSource =  new MatTableDataSource (data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.medicoService.mensajeCambio.subscribe(data =>{
      this.snackBar.open(data,'AVISO',{duration:2000});
    });

    this.medicoService.listar().subscribe(data =>{
      this.dataSource =  new MatTableDataSource (data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  eliminar(objMedico: Medico){
      this.medicoService.eliminar(objMedico.idMedico).subscribe(data =>{
          this.medicoService.listar().subscribe(listaMedicos =>{
            this.medicoService.medicoCambio.next(listaMedicos);
            this.medicoService.mensajeCambio.next('Se elimino');
          });
      });
  }
  openDialog(objMedico ?: Medico){
    let med = objMedico != null ? objMedico: new Medico();
    this.dialog.open(DialogoComponent,{
      width: '250px',
      disableClose:false,
      data : med
    });
  }
  applyFilter(filterValue:string){
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }


}
