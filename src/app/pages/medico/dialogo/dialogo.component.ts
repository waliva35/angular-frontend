import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Medico } from 'src/app/_model/Medico';
import { MedicoService } from 'src/app/_service/medico.service';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit {

  objMedico: Medico;
  constructor(private dialogRef:MatDialogRef<DialogoComponent>,
              @Inject(MAT_DIALOG_DATA) public data:Medico,
              private medicoService:MedicoService) { }

  ngOnInit(): void {
    //this.objMedico = this.data;
    this.objMedico = new Medico();
    this.objMedico.idMedico = this.data.idMedico;
    this.objMedico.nombres = this.data.nombres;
    this.objMedico.apellidos = this.data.apellidos;
    this.objMedico.cmp = this.data.cmp;
  }

  cancelar(){
    this.dialogRef.close();
  }

  operar(){
    if(this.objMedico!=null && this.objMedico.idMedico > 0){
      this.medicoService.actualizar(this.objMedico).subscribe(actualizar =>{
        this.medicoService.listar().subscribe(data =>{
          this.medicoService.medicoCambio.next(data);
          this.medicoService.mensajeCambio.next('Se actualizo');
        });
      });
    }else{
      this.medicoService.registrar(this.objMedico).subscribe(registrar =>{
        this.medicoService.listar().subscribe(data =>{
          this.medicoService.medicoCambio.next(data);
          this.medicoService.mensajeCambio.next('Se registro');
        });
      });
    }
    this.dialogRef.close();
  }


}
