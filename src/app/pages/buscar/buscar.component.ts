import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Consulta } from 'src/app/_model/Consulta';
import { FiltroConsulta } from 'src/app/_model/FiltroConsulta';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { DialogoDetalleComponent } from './dialogo-detalle/dialogo-detalle.component';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  form: FormGroup;

  displayedColumns = ['paciente', 'medico', 'especialidad', 'fecha', 'acciones'];
  dataSource: MatTableDataSource<Consulta>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  maxFecha: Date = new Date();

  constructor(private consultaService: ConsultaService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'dni': new FormControl(''),
      'nombreCompleto': new FormControl(''),
      'fechaConsulta': new FormControl()
    });
  }

  buscar(){

    let filtro = new FiltroConsulta(this.form.value['dni'],this.form.value['nombreCompleto'],this.form.value['fechaConsulta']);
    filtro.nombreCompleto = filtro.nombreCompleto.toLocaleLowerCase();

    if (filtro.fechaConsulta){
        filtro.fechaConsulta.setHours(0);
        filtro.fechaConsulta.setMinutes(0);
        filtro.fechaConsulta.setSeconds(0);
        filtro.fechaConsulta.setMilliseconds(0);

        delete filtro.dni;
        delete filtro.nombreCompleto;

        this.consultaService.buscar(filtro).subscribe(data =>{
            this.dataSource = new MatTableDataSource(data);
        });
    }else {
        delete filtro.fechaConsulta;
        if(filtro.dni.length === 0){
              delete filtro.dni;
        }
        if(filtro.nombreCompleto.length === 0){
          delete filtro.nombreCompleto
        }

        this.consultaService.buscar(filtro).subscribe(data =>{
          this.dataSource = new MatTableDataSource(data);
      });

    }


  }

  

  verDetalle(consulta: Consulta) {
    console.log(consulta);
    this.dialog.open(DialogoDetalleComponent, {      
      data: consulta
    });
  }


}
