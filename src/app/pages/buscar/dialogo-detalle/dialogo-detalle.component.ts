import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Consulta } from 'src/app/_model/Consulta';
import { ConsultaListaExamen } from 'src/app/_model/ConsultaListaExamen';
import { ExamenService } from 'src/app/_service/examen.service';

@Component({
  selector: 'app-dialogo-detalle',
  templateUrl: './dialogo-detalle.component.html',
  styleUrls: ['./dialogo-detalle.component.css']
})
export class DialogoDetalleComponent implements OnInit {

  consulta : Consulta;
  examenes:ConsultaListaExamen[];


  constructor(public dialogoRef:MatDialogRef<DialogoDetalleComponent>,
              @Inject(MAT_DIALOG_DATA) public data:Consulta,
              private examenService:ExamenService) { }

  ngOnInit(): void {
    this.consulta = this.data;
    this.listarExamenes();
  }

  listarExamenes(){
    this.examenService.listarExamenPorConsulta(this.consulta.idConsulta).subscribe(exam => {
        this.examenes = exam;
    });
  }

  cancelar(){
    this.dialogoRef.close();
  }

}
