import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Paciente } from 'src/app/_model/Paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
import { PacienteEdicionComponent } from './paciente-edicion/paciente-edicion.component';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  //pacientes: Paciente[] = [];
  dataSource:MatTableDataSource<Paciente>;
  displayedColumns = ['idPaciente','nombres','apellidos','acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  //inyeccion de dependecias _service/Paciente
  constructor(private pacienteService:PacienteService, 
              private snackBar:MatSnackBar,
              private dialog: MatDialog
              ) { }
  //xq no puedo utilizar el llenado de la lista en un constructor?la instancia
  //paciente esta disponible despues de la construccion del objeto, originando
  //una referencia de memoria.
  //ngOnInit: funcion que se ejecuta apenas el componente es creado
  ngOnInit(): void {
    //cuando venga informacion es el evento de suscribirse al metodo listar()
    
    this.pacienteService.pacienteCambio.subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.pacienteService.mensajeCambio.subscribe(data =>{
        this.snackBar.open(data,'AVISO',{duration:2000});
    });

    this.pacienteService.listar().subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  eliminar(idPaciente:number){
    this.pacienteService.eliminar(idPaciente).subscribe(data =>{
      this.pacienteService.listar().subscribe(pacientes => {
        this.pacienteService.pacienteCambio.next(pacientes);
        this.pacienteService.mensajeCambio.next('Se Elimino');
     });
    });
  }

  openDialog(paciente?: Paciente) {
    let pac = paciente != null ? paciente : new Paciente();
    this.dialog.open(PacienteEdicionComponent, {
      width: '500px',
      disableClose: false,
      data: pac
    });
  }

}
