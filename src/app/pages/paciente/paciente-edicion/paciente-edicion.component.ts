import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Paciente } from 'src/app/_model/Paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  id:number;
  form:FormGroup;
  edicion:boolean = false;
  objPaciente:Paciente;
  constructor(private route:ActivatedRoute,
              private dialogRef: MatDialogRef<PacienteEdicionComponent>,
              private router:Router,
              @Inject(MAT_DIALOG_DATA) public data: Paciente,
              private pacienteService:PacienteService) { }

  ngOnInit(): void {
    //creando y configurando un formulario

    this.objPaciente = new Paciente();

    this.objPaciente.idPaciente = this.data.idPaciente
    this.objPaciente.nombres = this.data.nombres
    this.objPaciente.apellidos = this.data.apellidos
    this.objPaciente.dni = this.data.dni
    this.objPaciente.direccion = this.data.direccion
    this.objPaciente.email = this.data.email
    this.objPaciente.telefono = this.data.telefono

    console.log(this.objPaciente);
    this.form = new FormGroup({
      'id': new FormControl(this.objPaciente.idPaciente),
      'nombres': new FormControl(this.objPaciente.nombres),
      'apellidos': new FormControl(this.objPaciente.apellidos),
      'dni': new FormControl(this.objPaciente.dni),
      'direccion': new FormControl(this.objPaciente.direccion),
      'telefono': new FormControl(this.objPaciente.telefono),
      'email': new FormControl(this.objPaciente.email),
    });
  }

  Operar(){
    this.objPaciente.idPaciente = this.form.value['id'];
    this.objPaciente.nombres = this.form.value['nombres'];
    this.objPaciente.apellidos = this.form.value['apellidos'];
    this.objPaciente.dni = this.form.value['dni'];
    this.objPaciente.direccion = this.form.value['direccion'];
    this.objPaciente.telefono = this.form.value['telefono'];
    this.objPaciente.email = this.form.value['email'];

    if(this.objPaciente != null && this.objPaciente.idPaciente > 0){
      //actualiza
       this.pacienteService.actualizar(this.objPaciente).subscribe(data =>{
         this.pacienteService.listar().subscribe(pacientes =>{
            this.pacienteService.pacienteCambio.next(pacientes);
            this.pacienteService.mensajeCambio.next('Se Actualizo')
         });
       });
    }else{
      //nuevo - registrar
      this.pacienteService.registrar(this.objPaciente).subscribe(data =>{
        this.pacienteService.listar().subscribe(pacientes =>{
           this.pacienteService.pacienteCambio.next(pacientes);
           this.pacienteService.mensajeCambio.next('Se Registro')

        });
      });
    }
    //para acceder desde el ts, a una ruta determinada, inyectando private router:Router
    //this.router.navigate(['paciente']);
    this.dialogRef.close();
  }

  cancelar() {
    //this.medicoService.confirmacion.next(false);
    this.dialogRef.close();
  }

}
