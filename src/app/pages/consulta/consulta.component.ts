import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Consulta } from 'src/app/_model/Consulta';
import { ConsultaListaExamen } from 'src/app/_model/ConsultaListaExamen';
import { DetalleConsulta } from 'src/app/_model/DetalleConsulta';
import { Especialidad } from 'src/app/_model/Especialidad';
import { Examen } from 'src/app/_model/Examen';
import { Medico } from 'src/app/_model/Medico';
import { Paciente } from 'src/app/_model/Paciente';
import { ConsultaService } from 'src/app/_service/consulta.service';
import { EspecialidadService } from 'src/app/_service/especialidad.service';
import { ExamenService } from 'src/app/_service/examen.service';
import { MedicoService } from 'src/app/_service/medico.service';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  pacientes:Paciente[] = [];
  medicos:Medico[] = [];
  especialidades:Especialidad[] = [];
  examenes:Examen[] = [];


  idPacienteSeleccionado:number;
  idMedicoSeleccionado:number;
  idEspecialidadSeleccionado:number;
  idExamenSeleccionado:number;

  fechaSeleccionada :Date = new Date();
  maxFecha: Date = new Date();

  diagnostico:string;
  tratamiento:string;
  mensaje:string;

  detalleConsulta:DetalleConsulta[] = [];
  examenesSeleccionados:Examen[] = [];

  constructor(private consultaService:ConsultaService,
              private pacienteService:PacienteService,
              private medicoService:MedicoService,
              private especialidadService:EspecialidadService,
              private examenService:ExamenService,
              private snackBar:MatSnackBar) 
              { }

  ngOnInit(): void {
    this.listarPacientes();
    this.listarMedicos();
    this.listarEspecialidad();
    this.listarExamen();
  }

  listarPacientes(){
    this.pacienteService.listar().subscribe(data =>{
      this.pacientes = data;
    });
  }

  listarMedicos(){
    this.medicoService.listar().subscribe(data =>{
      this.medicos = data;
    })
  }

  listarEspecialidad(){
    this.especialidadService.listar().subscribe(data =>{
      this.especialidades = data;
    })
  }

  listarExamen(){
    this.examenService.listar().subscribe(data =>{
      this.examenes = data;
    })
  }

  agregarDetalle(){
    if (this.diagnostico != null && this.tratamiento != null){
      let det = new DetalleConsulta();
      det.diagnostico = this.diagnostico;
      det.tratamiento = this.tratamiento;
      this.detalleConsulta.push(det);

      this.diagnostico = null;
      this.tratamiento = null;
    }else{
        this.mensaje ="Debe agregar un diagnostico y tratamiento";
        this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
    }
 
  }

  removerDiagnostico(index:number){
    this.detalleConsulta.splice(index,1);
  }

  removerExamen(index:number){
    this.examenesSeleccionados.splice(index,1);
  }

  estadoBotonRegistrar() {
    return (this.detalleConsulta.length === 0 || this.idEspecialidadSeleccionado === 0 || this.idMedicoSeleccionado === 0 || this.idPacienteSeleccionado === 0);
  }

  AgregarExamen(){
      if (this.idExamenSeleccionado > 0){

          let cont = 0;
          for (let i = 0; i < this.examenesSeleccionados.length; i++) {
              let examen = this.examenesSeleccionados[i];
              if (examen.idExamen === this.idExamenSeleccionado) {
                cont++;
                break;
              }
          }
          console.log(cont);

          if(cont > 0){
              this.mensaje="El examen se encuentra en la lista";
              this.snackBar.open(this.mensaje,"Aviso",{duration:2000});
          } else {
              let examen = new Examen();
              examen.idExamen = this.idExamenSeleccionado;
              console.log(examen.idExamen);
              this.examenService.listarExamenPorId(this.idExamenSeleccionado).subscribe(data =>{
                examen.nombre = data.nombre;
                this.examenesSeleccionados.push(examen);
              });
          }
      }else{
         this.mensaje="Debe agregar un examen";
         this.snackBar.open(this.mensaje,"AVISO",{duration:2000});
      }

  }

  RegistrarConsulta(){
      let objMedico = new Medico();
      objMedico.idMedico = this.idMedicoSeleccionado;
      
      let objEspecialidad= new Especialidad();
      objEspecialidad.idEspecialidad = this.idEspecialidadSeleccionado;
      
      let objPaciente = new Paciente();
      objPaciente.idPaciente = this.idPacienteSeleccionado;

      let objConsulta = new Consulta();
      objConsulta.especialidad = objEspecialidad;
      objConsulta.medico = objMedico;
      objConsulta.paciente = objPaciente;

      let tzoffset = (this.fechaSeleccionada).getTimezoneOffset() * 60000;
      let localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
      objConsulta.fecha = localISOTime;
      objConsulta.detalleConsulta = this.detalleConsulta;

      let consultaListaExamen = new ConsultaListaExamen();
      consultaListaExamen.consulta = objConsulta;
      consultaListaExamen.lstExamen = this.examenesSeleccionados;

      console.log(Consulta);

      this.consultaService.registrar(consultaListaExamen).subscribe(data =>{
        this.snackBar.open("Se registro","Aviso",{duration:2000});

        setTimeout(() => {
           this.limpiarControles();
        }, 2000);

      });

      

  }

  limpiarControles(){
    this.detalleConsulta =[];
    this.examenesSeleccionados = [];
    this.diagnostico = '';
    this.tratamiento = '';
    this.idPacienteSeleccionado = 0;
    this.idEspecialidadSeleccionado = 0;
    this.idMedicoSeleccionado = 0;
    this.idExamenSeleccionado = 0;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMinutes(0);
    this.mensaje = '';
  }

}
