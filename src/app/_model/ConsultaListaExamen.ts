import { ExamenComponent } from '../pages/examen/examen.component';
import { Consulta } from './Consulta';
import { Examen } from './Examen';

export class ConsultaListaExamen{
    public consulta:Consulta;
    public lstExamen: Examen[];

}