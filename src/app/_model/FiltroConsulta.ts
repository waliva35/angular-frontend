export class FiltroConsulta{
    public dni:string;
    public nombreCompleto:string;
    public fechaConsulta:Date;

    constructor(dni:string,nombreCompleto:string,fechaConsulta:Date){
        this.dni = dni;
        this.nombreCompleto = nombreCompleto;
        this.fechaConsulta = fechaConsulta;
    }
}