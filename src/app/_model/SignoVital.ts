import { Paciente } from "./Paciente"

export class SignoVital
{
    public idSigno : number;
    public paciente : Paciente;
    public fecha: string;
    public temperatura: number;
    public pulso: number;
    public ritmo: number;
}