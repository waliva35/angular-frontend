import { DetalleConsulta } from './DetalleConsulta';
import { Especialidad } from './Especialidad';
import { Medico } from './Medico';
import { Paciente } from './Paciente';

export class Consulta{
    public idConsulta:number;
    public paciente:Paciente;
    public medico:Medico;
    public especialidad: Especialidad;
    public fecha: string;
    public detalleConsulta: DetalleConsulta[];

}