import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Especialidad } from '../_model/Especialidad';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {

  url:string = `${HOST}/especialidades`;

  constructor(private http:HttpClient) { }

  listar(){
    return this.http.get<Especialidad[]>(this.url);
  }

  listarEspecialidadPorId(idEspecialidad:number){
    return this.http.get(`${this.url}/${idEspecialidad}`);
  }
  registrar(especialidad:Especialidad){
    return this.http.post(this.url,especialidad);
  }
  actualizar(especialidad:Especialidad){
    return this.http.post(this.url,especialidad);
  }
  Eliminar (idEspecialidad:number){
    return this.http.delete(`${this.url}/${idEspecialidad}`);
  }
}
