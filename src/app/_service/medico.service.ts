import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Medico } from '../_model/Medico';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  url:string = `${HOST}/medicos`;

  medicoCambio = new Subject<Medico[]>();
  mensajeCambio = new Subject<string>();

  constructor(private http:HttpClient) { }

  listar(){

     return this.http.get<Medico[]>(this.url);
  }
  listarMedicoPorId(idMedico:number){
    return this.http.get<Medico>(`${this.url}/${idMedico}`);
  }
  registrar(objMedico:Medico){
    return this.http.post(this.url,objMedico);
  }
  actualizar(objMedico:Medico){
    return this.http.post(this.url,objMedico);
  }

  eliminar(idMedico:number){
    return this.http.delete(`${this.url}/${idMedico}`);
  }





}
