import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Consulta } from '../_model/Consulta';
import { ConsultaListaExamen } from '../_model/ConsultaListaExamen';
import { FiltroConsulta } from '../_model/FiltroConsulta';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  url: string = `${HOST}/consultas`;
  constructor(private http:HttpClient) { }

  registrar(consultaDTO:ConsultaListaExamen){
    return this.http.post(this.url,consultaDTO);
  }

  buscar(filtroConsulta:FiltroConsulta){
    return this.http.post<Consulta[]>(`${this.url}/buscar`,filtroConsulta);
  }
  

}
