import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Paciente } from '../_model/Paciente';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  url: string = `${HOST}/pacientes`;
  //variables de transporte de informacion de un componente a otro
  //siendo observables
  pacienteCambio = new Subject<Paciente[]>();
  mensajeCambio = new Subject<string>();


  //se define como parametro inyeccion de dependecias el 
  //tipo de dato HttpClient
  constructor(private http:HttpClient) { }

  listar(){
    //de la clases http, se obtiene el get, post,put,delete
    //get que va devolver : Paciente[]
    //como parametro (url)
      return this.http.get<Paciente[]>(this.url);
  }

  listarPacientePorId(id:number){
      return this.http.get<Paciente>(`${this.url}/${id}`);
  }

  registrar(paciente:Paciente){
    return this.http.post(this.url,paciente);
  }

  actualizar(paciente:Paciente){
    return this.http.put(this.url,paciente);
  }

  eliminar(id:number){
    return this.http.delete(`${this.url}/${id}`);
  }


}
