import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SignoVital } from '../_model/SignoVital';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class SignovitalService {

  url:string = `${HOST}/signovital`;

  signovitalCambio = new Subject<SignoVital[]>();
  mensajeCambio = new Subject<string>();

  constructor(private http:HttpClient) { }

  listar(){
    return this.http.get<SignoVital[]>(this.url);
  }

  listarSignoVitalPorId(idSignov:number){
    return this.http.get<SignoVital>(`${this.url}/${idSignov}`);
  }

  registrar(signov:SignoVital){
    return this.http.post(this.url,signov);
  }
  
  actualizar(signov:SignoVital){
    return this.http.put(this.url,signov);
  }

  eliminar(idSignov:number){
    return this.http.delete(`${this.url}/${idSignov}`);

  }

}
