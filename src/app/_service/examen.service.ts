import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConsultaListaExamen } from '../_model/ConsultaListaExamen';
import { Examen } from '../_model/Examen';
import { HOST } from '../_shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {

  url:string = `${HOST}/examenes`;
  constructor(private http:HttpClient) { }

  listar(){
    return this.http.get<Examen[]>(this.url);
  }

  listarExamenPorId(idExamen:number){
    return this.http.get<Examen>(`${this.url}/${idExamen}`);
  }
  registrar(examen:Examen){
    return this.http.post(this.url,examen);
  }
  actualizar(examen:Examen){
    return this.http.post(this.url,examen);
  }
  Eliminar (idExamen:number){
    return this.http.delete(`${this.url}/${idExamen}`);
  }
  listarExamenPorConsulta(idConsulta:number){
    return this.http.get<ConsultaListaExamen[]>(`${HOST}/consultaexamenes/${idConsulta}`);
  }

}
