import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuscarComponent } from './pages/buscar/buscar.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialFormComponent } from './pages/consulta/especial-form/especial-form.component';
import { EspecialidadEdicionComponent } from './pages/especialidad/especialidad-edicion/especialidad-edicion.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenEdicionComponent } from './pages/examen/examen-edicion/examen-edicion.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { PacienteEdicionComponent } from './pages/paciente/paciente-edicion/paciente-edicion.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { SignovitalEdicionComponent } from './pages/signovital/signovital-edicion/signovital-edicion.component';
import { SignovitalComponent } from './pages/signovital/signovital.component';

const routes: Routes = [
  {
    path:'paciente',component:PacienteComponent,children:[
      { path:'nuevo',component:PacienteEdicionComponent},
      { path:'edicion/:id',component:PacienteEdicionComponent}
    ]
  },
  {
    path:'especialidad',component:EspecialidadComponent,children:[
      { path:'nuevo',component:EspecialidadEdicionComponent},
      { path:'edicion/:id',component:EspecialidadEdicionComponent}
    ]
  },
  {
    path:'examen',component:ExamenComponent,children:[
      { path:'nuevo',component:ExamenEdicionComponent},
      { path:'edicion/:id',component:ExamenEdicionComponent}
    ]
  },
  {
    path:'signovital',component:SignovitalComponent,children:[
      { path:'nuevo',component:SignovitalEdicionComponent},
      { path:'edicion/:id',component:SignovitalEdicionComponent}
    ]
  },
  {path:'medico',component:MedicoComponent},
  {path:'consulta',component:ConsultaComponent},
  {path:'consulta-especial',component:EspecialFormComponent},
  {path:'buscar',component:BuscarComponent},
  {path:'reporte',component:ReporteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
